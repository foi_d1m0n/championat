//
//  UploadingViewController.m
//  Championat
//
//  Created by Mac on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UploadingViewController.h"
#import "AnimationCoreModel.h"
#import "ChampMenuViewController.h"

@interface UploadingViewController ()

@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic ,strong) IBOutlet UILabel *uploadingisDoneMessage;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIImageView *Ball;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

@end

@implementation UploadingViewController
@synthesize Ball;
@synthesize backgroundImage;
@synthesize arrow;
@synthesize progressBar = _progressBar;

@synthesize cancelButton = _cancelButton;
@synthesize uploadingisDoneMessage = _uploadingisDoneMessage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.cancelButton setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    
    self.uploadingisDoneMessage.text = NSLocalizedString(@"uploadingVideoLabel", nil);
    
    UIFont *font =[UIFont fontWithName:@"Bauhaus Cyrillic" size:35];
    [self.uploadingisDoneMessage setFont:font];    
    [self.cancelButton setFont:font];
    
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceUp || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceDown) {
        CGRect rect=self.view.frame;
        [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+266 y:rect.size.height-86];
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.cancelButton, nil] duration:0.125 x:0.8 y:0.8];
    }
    
    [AnimationCoreModel animateBallElement:Ball duration:5.0];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setProgressBar:nil];
    [self setBall:nil];
    [self setBackgroundImage:nil];
    [self setArrow:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)cancelButtonClick:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];
    
    
    NSArray *controllers = [self.navigationController childViewControllers];

    for (UIViewController *vc in controllers) 
    {
        if ([vc isKindOfClass:[ChampMenuViewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
    
}

- (IBAction)infoButtonPressed:(id)sender 
{
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];
    
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"infoViewController"] animated:YES];
}

- (IBAction)backButtonPressed:(id)sender 
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonePressed:(id)sender 
{
    [self.navigationController popToViewController:[self.navigationController.childViewControllers objectAtIndex:0] animated:YES];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (toInterfaceOrientation==UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation==UIDeviceOrientationLandscapeRight ||
        toInterfaceOrientation==UIDeviceOrientationFaceUp ||
        toInterfaceOrientation==UIDeviceOrientationFaceDown)
    {
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.cancelButton, nil] duration:0.125 x:0.8 y:0.8];
    }
    else {
        backgroundImage.image=[UIImage imageNamed:@"fon_1.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.cancelButton, nil] duration:0.125 x:1.0 y:1.0];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    CGRect rect=self.view.frame;
    [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+366 y:rect.size.height-86];
}
@end
