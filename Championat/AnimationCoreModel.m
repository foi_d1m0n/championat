//
//  AnimationCoreModel.m
//  Championat
//
//  Created by Mac on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AnimationCoreModel.h"
#import "ChampViewController.h"
#define P(x,y) CGPointMake(x, y)
#import <QuartzCore/QuartzCore.h>

@implementation AnimationCoreModel
@synthesize ballImagesArray=_ballImagesArray;

+(NSArray *)loadBall
{
    
    
    return [NSArray arrayWithObjects:
                            
                            [UIImage imageNamed:@"2.png"],
                            [UIImage imageNamed:@"3.png"],
                            [UIImage imageNamed:@"4.png"],
                            [UIImage imageNamed:@"5.png"],
                            [UIImage imageNamed:@"6.png"],
                            [UIImage imageNamed:@"7.png"],
                            [UIImage imageNamed:@"8.png"],
                            [UIImage imageNamed:@"9.png"],
                            [UIImage imageNamed:@"10.png"],
                            [UIImage imageNamed:@"11.png"],
                            [UIImage imageNamed:@"12.png"],
                            [UIImage imageNamed:@"13.png"],
                            [UIImage imageNamed:@"14.png"],
                            [UIImage imageNamed:@"15.png"],
                            [UIImage imageNamed:@"16.png"],
                            [UIImage imageNamed:@"17.png"],
                            [UIImage imageNamed:@"18.png"],
                            [UIImage imageNamed:@"19.png"],
                            [UIImage imageNamed:@"20.png"],
                            [UIImage imageNamed:@"21.png"],
                            [UIImage imageNamed:@"22.png"],
                            [UIImage imageNamed:@"23.png"],
                            [UIImage imageNamed:@"24.png"],
                            [UIImage imageNamed:@"25.png"],
                            [UIImage imageNamed:@"26.png"],
                            [UIImage imageNamed:@"27.png"],
                            [UIImage imageNamed:@"28.png"],
                            [UIImage imageNamed:@"29.png"],
                            [UIImage imageNamed:@"30.png"],
                            [UIImage imageNamed:@"31.png"],
                            [UIImage imageNamed:@"32.png"],
                            [UIImage imageNamed:@"33.png"],
                            [UIImage imageNamed:@"34.png"],
                            [UIImage imageNamed:@"35.png"],
                            [UIImage imageNamed:@"36.png"],
                            [UIImage imageNamed:@"37.png"],
                            [UIImage imageNamed:@"38.png"],
                            [UIImage imageNamed:@"39.png"],
                            [UIImage imageNamed:@"40.png"],
                            [UIImage imageNamed:@"41.png"],
                            [UIImage imageNamed:@"42.png"],
                            [UIImage imageNamed:@"43.png"],
                            [UIImage imageNamed:@"44.png"],
                            [UIImage imageNamed:@"45.png"],
                            [UIImage imageNamed:@"46.png"],
                            [UIImage imageNamed:@"47.png"],
                            [UIImage imageNamed:@"48.png"],
                            [UIImage imageNamed:@"49.png"],
                            [UIImage imageNamed:@"50.png"],
                            [UIImage imageNamed:@"51.png"],
                            [UIImage imageNamed:@"52.png"],
                            [UIImage imageNamed:@"53.png"],
                            [UIImage imageNamed:@"54.png"],
                            [UIImage imageNamed:@"55.png"],
                            [UIImage imageNamed:@"56.png"],
                            [UIImage imageNamed:@"57.png"],
                            [UIImage imageNamed:@"58.png"],
                            [UIImage imageNamed:@"59.png"],
                            [UIImage imageNamed:@"60.png"],
                            [UIImage imageNamed:@"61.png"],
                            [UIImage imageNamed:@"62.png"],
                            [UIImage imageNamed:@"63.png"],
                            [UIImage imageNamed:@"64.png"],
                            [UIImage imageNamed:@"65.png"],
                            [UIImage imageNamed:@"66.png"],
                            [UIImage imageNamed:@"67.png"],
                            [UIImage imageNamed:@"68.png"],
                            [UIImage imageNamed:@"69.png"],
                            [UIImage imageNamed:@"70.png"],
                            [UIImage imageNamed:@"71.png"],
                            [UIImage imageNamed:@"72.png"],
                            [UIImage imageNamed:@"73.png"],
                            [UIImage imageNamed:@"74.png"],
                            [UIImage imageNamed:@"75.png"],
                            [UIImage imageNamed:@"76.png"],
                            [UIImage imageNamed:@"77.png"],
                            [UIImage imageNamed:@"78.png"],
                            [UIImage imageNamed:@"79.png"],
                            [UIImage imageNamed:@"80.png"],
                            [UIImage imageNamed:@"81.png"],
                            [UIImage imageNamed:@"82.png"],
                            [UIImage imageNamed:@"83.png"],
                            [UIImage imageNamed:@"84.png"],
                            [UIImage imageNamed:@"85.png"],
                            [UIImage imageNamed:@"86.png"],
                            [UIImage imageNamed:@"87.png"],
                            [UIImage imageNamed:@"88.png"],
                            [UIImage imageNamed:@"89.png"],
                            [UIImage imageNamed:@"90.png"],
                            [UIImage imageNamed:@"91.png"],
                            [UIImage imageNamed:@"92.png"],
                            [UIImage imageNamed:@"93.png"],
                            [UIImage imageNamed:@"94.png"],
                            [UIImage imageNamed:@"95.png"],
                            [UIImage imageNamed:@"96.png"],
                            [UIImage imageNamed:@"97.png"],
                            [UIImage imageNamed:@"98.png"],
                            [UIImage imageNamed:@"99.png"],
                            [UIImage imageNamed:@"100.png"],nil];
}

+(void)fadeTimingAnimation:(id)sender duration:(double)duration repeatCount:(double)repeatCount autoreverses:(BOOL)autoreverses 
{
    UIView *senderView = (UIView*)sender;
    if (![senderView isKindOfClass:[UIView class]])
    return;
    [UIView beginAnimations:nil context:nil];
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform"];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = duration;
    anim.repeatCount = repeatCount;
    anim.autoreverses = autoreverses;
    anim.removedOnCompletion = YES;
    anim.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)];
    [senderView.layer addAnimation:anim forKey:nil];
    [UIView commitAnimations];
}

//Make transparent controlls in specificated time interval
+(void)fadeInAllElements:(NSArray *)objects duration:(double)duration alpha:(double)alpha
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    for (int k=0; k<objects.count; k++) {
        UIView *senderView = (UIView*)[objects objectAtIndex:k];
        if ([senderView isKindOfClass:[UIView class]])
            senderView.alpha=alpha;
    }
    [UIView commitAnimations];
}

+(void)moveElement:(id)sender duration:(double)duration x:(double)x y:(double)y
{
    UIView *senderView = (UIView*)sender;
    if (![senderView isKindOfClass:[UIView class]])
        return;
    [UIView beginAnimations:@"Move" context:nil];
    [UIView setAnimationDuration:duration];
    senderView.center=CGPointMake(x, y);
    [UIView commitAnimations];
}

+(void)animateBallElement:(id)sender duration:(double)duration
{
    UIImageView *senderView = (UIImageView*)sender;
    if (![senderView isKindOfClass:[UIImageView class]])
        return;
    senderView.animationImages=[self loadBall];
    senderView.animationDuration=duration;
    [senderView startAnimating];
}

+(void)questionAnimationOn:(NSArray *)objects duration:(double)duration x:(double)x y:(double)y alpha:(double)alpha
{
    [UIView animateWithDuration:duration
                     animations:^{
                         UIView *senderView = (UIView*)[objects objectAtIndex:0];
                         if ([senderView isKindOfClass:[UIView class]])
                             if([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceUp || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceDown)
                             {
                                 senderView.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(x-0.2, y-0.2), CGAffineTransformMakeTranslation(0.0, 0.0));
                             } else
                                 senderView.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(x, y), CGAffineTransformMakeTranslation(0.0, 0.0));
                         senderView.alpha=alpha;
                         UIView *senderViewNext = (UIView*)[objects objectAtIndex:1];
                         if ([senderViewNext isKindOfClass:[UIView class]])
                             senderViewNext.alpha=alpha;
                     }];
}

+(void)questionAnimationOff:(NSArray *)objects duration:(double)duration x:(double)x y:(double)y alpha:(double)alpha
{
    [UIView animateWithDuration:duration
                     animations:^{
                         UIView *senderView = (UIView*)[objects objectAtIndex:0];
                         if ([senderView isKindOfClass:[UIView class]])
                             if([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceUp || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceDown)
                             {
                                 senderView.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(x-0.2, y-0.2), CGAffineTransformMakeTranslation(0.0, 0.0));
                             } else
                                 senderView.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(x, y), CGAffineTransformMakeTranslation(0.0, 0.0));
                         senderView.alpha=alpha;
                         UIView *senderViewNext = (UIView*)[objects objectAtIndex:1];
                         if ([senderViewNext isKindOfClass:[UIView class]])
                             senderViewNext.alpha=alpha;
                     }];
}

+(void)setScaleForElement:(NSArray *)objects duration:(double)duration x:(double)x y:(double)y
{
    [UIView animateWithDuration:duration
                     animations:^{
                         for (int k=0; k<objects.count; k++) {
                             UIView *senderView = (UIView*)[objects objectAtIndex:k];
                             if ([senderView isKindOfClass:[UIView class]])
                                 senderView.transform = CGAffineTransformConcat(CGAffineTransformMakeScale(x, y), CGAffineTransformMakeTranslation(0.0, 0.0));
                         }
                     }];
}





@end
