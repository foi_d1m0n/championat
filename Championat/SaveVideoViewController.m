//
//  SaveVideoViewController.m
//  Championat
//
//  Created by Mac on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SaveVideoViewController.h"
#import "AnimationCoreModel.h"

@interface SaveVideoViewController ()

@property (nonatomic, strong) IBOutlet UILabel *enterYourNameLabel;
@property (nonatomic, strong) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIImageView *Ball;
@property (strong, nonatomic) IBOutlet UITextField *textFieldLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

@end

@implementation SaveVideoViewController

@synthesize backgroundImage;
@synthesize arrow;

@synthesize enterYourNameLabel = _enterYourNameLabel;
@synthesize saveButton = _saveButton;
@synthesize Ball;
@synthesize textFieldLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.textFieldLabel.frame=CGRectMake(self.textFieldLabel.frame.origin.x, self.textFieldLabel.frame.origin.y-20, self.textFieldLabel.frame.size.width, 44);
    [self.saveButton setTitle:NSLocalizedString(@"saveVideo", nil) forState:UIControlStateNormal];
    self.enterYourNameLabel.text = NSLocalizedString(@"enterYourName", nil);
    
    UIFont *font =[UIFont fontWithName:@"Bauhaus Cyrillic" size:35];
    [self.saveButton setFont:font];    
    [self.textFieldLabel setFont:font];
    [self.enterYourNameLabel setFont:font];
    
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceUp || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceDown) {
        CGRect rect=self.view.frame;
        [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+266 y:rect.size.height-86];
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.saveButton,self.textFieldLabel, nil] duration:0.125 x:0.8 y:0.8];
    }
    
    [AnimationCoreModel animateBallElement:Ball duration:5];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setBall:nil];
    [self setBackgroundImage:nil];
    [self setArrow:nil];
    [self setTextFieldLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)saveButtonClick:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];
}

- (IBAction)infoButtonPressed:(id)sender 
{
    
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];
    
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"infoViewController"] animated:YES];
}

- (IBAction)backButtonPressed:(id)sender 
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonePressed:(id)sender 
{
    [self.navigationController popToViewController:[self.navigationController.childViewControllers objectAtIndex:0] animated:YES];
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (toInterfaceOrientation==UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation==UIDeviceOrientationLandscapeRight ||
        toInterfaceOrientation==UIDeviceOrientationFaceUp ||
        toInterfaceOrientation==UIDeviceOrientationFaceDown)
    {
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.saveButton,self.textFieldLabel, nil] duration:0.125 x:0.8 y:0.8];
    }
    else {
        backgroundImage.image=[UIImage imageNamed:@"fon_1.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.saveButton,self.textFieldLabel, nil] duration:0.125 x:1.0 y:1.0];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    CGRect rect=self.view.frame;
    [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+366 y:rect.size.height-86];
}


- (BOOL)textFieldShouldReturn:(UITextField *) theTextField
{
    if (theTextField == self.textFieldLabel)
    {
        [theTextField resignFirstResponder];
    }
    
    return YES;
}

@end
