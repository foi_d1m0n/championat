//
//  ChampMenuViewController.h
//  Championat
//
//  Created by Mac on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MPMediaPlayback.h>

@interface ChampMenuViewController : UIViewController <MPMediaPlayback>
- (IBAction)showMyVideo:(id)sender;
- (IBAction)rewriteVideo:(id)sender;
- (IBAction)saveVideo:(id)sender;
- (IBAction)info:(id)sender;
@property NSArray *ballImagesArray;
@property (weak, nonatomic) IBOutlet UIImageView *Ball;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

@property (nonatomic, strong) NSURL *movieURL;

@end
