//
//  ChampInfoViewController.m
//  Championat
//
//  Created by Mac on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChampInfoViewController.h"
#import "AnimationCoreModel.h"

@interface ChampInfoViewController ()

@end

@implementation ChampInfoViewController
@synthesize Ball;
@synthesize backgroundImage;
@synthesize arrow;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceUp || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceDown) {
        CGRect rect=self.view.frame;
        [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+266 y:rect.size.height-86];
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
    }
    
    [AnimationCoreModel animateBallElement:Ball duration:5.0];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setBall:nil];
    [self setBackgroundImage:nil];
    [self setArrow:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (toInterfaceOrientation==UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation==UIDeviceOrientationLandscapeRight)
    {
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
    }
    else {
        backgroundImage.image=[UIImage imageNamed:@"fon_1.png"];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    CGRect rect=self.view.frame;
    [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+366 y:rect.size.height-86];
}
- (IBAction)backButtonPressed:(id)sender 
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)homeButtonePressed:(id)sender 
{
    [self.navigationController popToViewController:[self.navigationController.childViewControllers objectAtIndex:0] animated:YES];
}

@end
