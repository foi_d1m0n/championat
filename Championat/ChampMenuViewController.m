//
//  ChampMenuViewController.m
//  Championat
//
//  Created by Mac on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChampMenuViewController.h"
#import "AnimationCoreModel.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MediaPlayer/MPMoviePlayerViewController.h>

@interface ChampMenuViewController ()

{
    MPMoviePlayerController* theMovie;
}

@property (nonatomic, strong) IBOutlet UIButton *watchVideoButton;
@property (nonatomic, strong) IBOutlet UIButton *captureAnotherVideoButton;
@property (nonatomic, strong) IBOutlet UIButton *saveVideoButton;


@end

@implementation ChampMenuViewController



//MPPlayer properties 

@synthesize isPreparedToPlay;
@synthesize currentPlaybackRate;
@synthesize currentPlaybackTime;

@synthesize movieURL = _movieURL;
@synthesize Ball;
@synthesize backgroundImage;
@synthesize arrow;
@synthesize ballImagesArray;
@synthesize watchVideoButton = _watchVideoButton;
@synthesize captureAnotherVideoButton = _captureAnotherVideoButton;
@synthesize saveVideoButton = _saveVideoButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont *font =[UIFont fontWithName:@"Bauhaus Cyrillic" size:35];
    [self.watchVideoButton setFont:font];    
    [self.captureAnotherVideoButton setFont:font];
    [self.saveVideoButton setFont:font];
    
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceUp || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceDown) {
        CGRect rect=self.view.frame;
        [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+266 y:rect.size.height-86];
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.watchVideoButton,self.captureAnotherVideoButton,self.saveVideoButton, nil] duration:0.125 x:0.8 y:0.8];
    }
    
    [self.watchVideoButton setTitle:NSLocalizedString(@"watchVideoButton", nil) forState:UIControlStateNormal];
    
    [self.captureAnotherVideoButton setTitle:NSLocalizedString(@"captureAgainButton", nil) forState:UIControlStateNormal];
    [self.saveVideoButton setTitle:NSLocalizedString(@"iLikeItAndSave", nil) forState:UIControlStateNormal];

    
    [AnimationCoreModel animateBallElement:Ball duration:5.0];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:theMovie];
    
    [self setBackgroundImage:nil];
    [self setArrow:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)showMyVideo:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];    
    
    [self playMovieAtURL:self.movieURL];
}

-(void)playMovieAtURL:(NSURL*)theURL 
{
    if (!theURL)
    {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Failed to launch" message:@"Video hasn't captured yet!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertview show];
        
    }else 
    {
       
    theMovie = [[MPMoviePlayerController alloc] initWithContentURL:theURL]; 
    theMovie.scalingMode=MPMovieScalingModeAspectFill; 
    [theMovie.view setFrame:self.view.bounds];
    [self.view addSubview:[theMovie view]];
    
    [theMovie setContentURL:self.movieURL];       
    [theMovie prepareToPlay];
    [theMovie setFullscreen:YES];
    theMovie.useApplicationAudioSession = NO;
    theMovie.shouldAutoplay = YES;
    theMovie.controlStyle = MPMovieControlStyleDefault;
    
    [theMovie setMovieSourceType:MPMovieSourceTypeFile];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(moviewPlayBackWillExitFullScreen:) 
                                                 name:MPMoviePlayerWillExitFullscreenNotification 
                                               object:theMovie];
    
    [theMovie play]; 
    
    }
} 

- (void) moviewPlayBackWillExitFullScreen: (NSNotification*)notification
{
    [theMovie stop];
    [theMovie.view removeFromSuperview];
    
}

- (IBAction)rewriteVideo:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES]; 
    
    //[self backButtonPressed:sender];
}

- (IBAction)saveVideo:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES]; 
}

- (IBAction)info:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES]; 
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (toInterfaceOrientation==UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation==UIDeviceOrientationLandscapeRight ||
        toInterfaceOrientation==UIDeviceOrientationFaceUp ||
        toInterfaceOrientation==UIDeviceOrientationFaceDown)
    {
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.watchVideoButton,self.saveVideoButton,self.captureAnotherVideoButton, nil] duration:0.125 x:0.8 y:0.8];
    }
    else {
        backgroundImage.image=[UIImage imageNamed:@"fon_1.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:self.watchVideoButton,self.saveVideoButton,self.captureAnotherVideoButton, nil] duration:0.125 x:1.0 y:1.0];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    CGRect rect=self.view.frame;
    [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+366 y:rect.size.height-86];
}

- (IBAction)infoButtonPressed:(id)sender 
{
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"infoViewController"] animated:YES];
}

- (IBAction)backButtonPressed:(id)sender 
{
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonePressed:(id)sender 
{
    [self.navigationController popToViewController:[self.navigationController.childViewControllers objectAtIndex:0] animated:YES];
}

#pragma mark MPMediaPlayback Protocol

- (void) play
{
    
}

-(void) stop
{
    
}

-(void) pause
{
    
}

-(void) prepareToPlay
{
    
}

- (void) beginSeekingForward
{
    
}

- (void) beginSeekingBackward
{
    
}

-(void) endSeeking
{
    
}














@end
