//
//  AnimationCoreModel.h
//  Championat
//
//  Created by Mac on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnimationCoreModel : NSObject
@property (nonatomic, weak) NSArray *ballImagesArray;

+(void)fadeTimingAnimation:(id)sender duration:(double)duration repeatCount:(double)repeatCount autoreverses:(BOOL)autoreverses;
+(void)fadeInAllElements:(NSArray *)objects duration:(double)duration alpha:(double)alpha;
+(void)moveElement:(id)sender duration:(double)duration x:(double)x y:(double)y;
+(void)animateBallElement:(id)sender duration:(double)duration;
+(void)questionAnimationOn:(NSArray *)objects duration:(double)duration x:(double)x y:(double)y alpha:(double)alpha;
+(void)questionAnimationOff:(NSArray *)objects duration:(double)duration x:(double)x y:(double)y alpha:(double)alpha;
+(void)setScaleForElement:(NSArray *)objects duration:(double)duration x:(double)x y:(double)y;
@end
