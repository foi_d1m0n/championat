//
//  ChampViewController.m
//  Championat
//
//  Created by Mac on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChampViewController.h"
#import "AnimationCoreModel.h"
@interface ChampViewController ()
@property NSTimer *timer1Sec;
@property BOOL startingAnimationEnded;
@end

@implementation ChampViewController
@synthesize Ball;
@synthesize arrow;
@synthesize volna;
@synthesize info;
@synthesize insert;
@synthesize insertLabel;
@synthesize backGroundImage;
@synthesize timer3Sec=_timer3Sec;
@synthesize timer1Sec=_timer1Sec;
@synthesize startingAnimationEnded;


- (IBAction)infoButton:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];
    
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"infoViewController"] animated:YES];
    
}

- (IBAction)insertButton:(id)sender {
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];
}

-(void)setTimer3Sec:(NSTimer *)timer3Sec
{
    _timer3Sec=timer3Sec;
}

-(void)changeTimer1
{
    static int count1=0;
    switch (count1) {
        case 0:
        {
            [AnimationCoreModel fadeInAllElements:[NSArray arrayWithObjects:Ball,volna, nil] duration:1.0 alpha:1.0];
            count1++;
        }
            break;
            
        case 1:
        {
            [self setTimer3Sec:[NSTimer scheduledTimerWithTimeInterval:0.5
                                                                target:self
                                                              selector:@selector(changeTimer)
                                                              userInfo:nil 
                                                               repeats:YES]];
            count1++;
        }
            break;
            
        case 2:
        {
            CGRect rect=self.view.frame;
            [AnimationCoreModel moveElement:info duration:0.5 x:rect.size.width-info.frame.size.width/2 y:rect.size.height-info.frame.size.height/2];
            count1++;
        }
            break;
            
        case 3:
        {
            [AnimationCoreModel fadeInAllElements:[NSArray arrayWithObjects:insert,insertLabel, nil] duration:1.0 alpha:1.0];
            count1++;
        }
            break;
            
        default:
            [self.timer1Sec invalidate];
            self.timer1Sec = nil;
            count1=0;
            break;
    }
}



-(void)changeTimer{
    static int count=0;
    CGRect rect=self.view.frame;
    if(count==0){
        [AnimationCoreModel moveElement:arrow duration:0.5 x:rect.origin.x+366 y:rect.size.height-86];
        [self.timer3Sec invalidate];
        self.timer3Sec=nil;
        [self setTimer3Sec:[NSTimer scheduledTimerWithTimeInterval:0.125
                                                            target:self
                                                          selector:@selector(changeTimer)
                                                          userInfo:nil 
                                                           repeats:YES]];        
        count++;
    }else if (count==4) {
        [AnimationCoreModel moveElement:arrow duration:0.25 x:rect.origin.x+466 y:rect.size.height-86];
        count++;
    }else if (count==5) {
        [AnimationCoreModel moveElement:arrow duration:0.25 x:rect.origin.x+366 y:rect.size.height-86];
        [self.timer3Sec invalidate];
        self.timer3Sec=nil;
        count=0;
        startingAnimationEnded=YES;
    } else count++;  
    
}

- (void)viewDidLoad
{
    startingAnimationEnded=NO;
    [super viewDidLoad];
    //NSArray *fontarray = [UIFont fontNamesForFamilyName:@"BauhausCTT"];
    UIFont *font =[UIFont fontWithName:@"Bauhaus Cyrillic" size:35];
    [insertLabel setFont:font];
    font =[UIFont fontWithName:@"Bauhaus Cyrillic" size:35];
    [insert setFont:font];
    
    [self.insert setTitle:NSLocalizedString(@"participateButtonText", nil)
 forState:UIControlStateNormal];
    
    self.insertLabel.text = NSLocalizedString(@"Welome to EURO 2012", nil);
    
    self.volna.alpha = 0;
    self.Ball.alpha = 0;
    self.arrow.alpha = 1;
    self.info.alpha = 1;
    self.insert.alpha = 0;
    self.insert.titleLabel.numberOfLines = 2;
    self.insertLabel.alpha = 0;
    
    self.timer1Sec=[NSTimer scheduledTimerWithTimeInterval:1.0
                                                        target:self
                                                      selector:@selector(changeTimer1)
                                                      userInfo:nil 
                                                       repeats:YES];
    
    [AnimationCoreModel animateBallElement:Ball duration:5];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)viewDidUnload
{
    [self setBall:nil];
    [self setArrow:nil];
    [self setVolna:nil];
    [self setInfo:nil];
    [self setInsert:nil];
    [self setInsertLabel:nil];
    [self setBackGroundImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
        if (toInterfaceOrientation==UIDeviceOrientationLandscapeLeft ||
            toInterfaceOrientation==UIDeviceOrientationLandscapeRight)
        {
            backGroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        }
        else {
            backGroundImage.image=[UIImage imageNamed:@"fon_1.png"];
        }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (startingAnimationEnded) {
        CGRect rect=self.view.frame;
        [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+366 y:rect.size.height-86];
    }
}

@end
