//
//  ChampViewController.h
//  Championat
//
//  Created by Mac on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChampViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *Ball;
@property (nonatomic,weak) NSTimer *timer3Sec;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UIImageView *volna;
@property (weak, nonatomic) IBOutlet UIButton *info;
@property (weak, nonatomic) IBOutlet UIButton *insert;
@property (weak, nonatomic) IBOutlet UILabel *insertLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImage;
- (IBAction)infoButton:(id)sender;
- (IBAction)insertButton:(id)sender;

-(void)setTimer3Sec:(NSTimer *)timer3Sec;
-(void)changeTimer;

@end
