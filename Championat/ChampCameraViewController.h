//
//  ChampCameraViewController.h
//  Championat
//
//  Created by Mac on 5/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CMBufferQueue.h>

@interface ChampCameraViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, UINavigationControllerDelegate>

{
    AVCaptureSession *captureSession;
	AVCaptureConnection *audioConnection;
	AVCaptureConnection *videoConnection;
	
	NSURL *movieURL;
	AVAssetWriter *assetWriter;
	AVAssetWriterInput *assetWriterAudioIn;
	AVAssetWriterInput *assetWriterVideoIn;
    dispatch_queue_t movieWritingQueue;
    CMBufferQueueRef previewBufferQueue;
    
    BOOL readyToRecordAudio; 
    BOOL readyToRecordVideo;
    
    BOOL isRecordingNeedsToPause;
    
    int startCount;
    BOOL startVideoRecord;
    int time;
    int delay;
    int question;
    
    int seconds;
    int miliseconds;
    
    
}

@property NSArray *ballImagesArray;
@property NSArray *cameraImages;
@property (nonatomic) BOOL nextQuestion;
@property (nonatomic,strong) NSArray *elementsArray;
@property (nonatomic,strong) NSArray *questionsArray;

@property (weak, nonatomic) IBOutlet UIImageView *Ball;
@property (weak, nonatomic) IBOutlet UIImageView *myFrame;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIcon;
@property (weak, nonatomic) IBOutlet UILabel *secondToStart;
@property (weak, nonatomic) IBOutlet UIImageView *infoCircle;
@property (weak, nonatomic) IBOutlet UIImageView *infoRect;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UILabel *questionOne;
@property (weak, nonatomic) IBOutlet UILabel *questionTwo;
@property (weak, nonatomic) IBOutlet UILabel *questionThree;
@property (weak, nonatomic) IBOutlet UILabel *questionFour;
@property (weak, nonatomic) IBOutlet UIButton *questOne;
@property (weak, nonatomic) IBOutlet UIButton *questTwo;
@property (weak, nonatomic) IBOutlet UIButton *questThree;
@property (weak, nonatomic) IBOutlet UIButton *questFour;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;


- (IBAction)infoButtonClick:(id)sender;

-(void)changeTimer;
-(void)questionLoader;

@end
