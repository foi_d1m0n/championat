//
//  ChampCameraViewController.m
//  Championat
//
//  Created by Mac on 5/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChampCameraViewController.h"
#import "ChampMenuViewController.h"
#import "MobileCoreServices/MobileCoreServices.h"
#import <UIkit/UIKit.h>
#import "AnimationCoreModel.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define QUESTIONTIME 7;
#define DELAY 2;
#define STARTTIME 3;
#define QUESTIONCOUNT 4;

@interface ChampCameraViewController () 

@property NSTimer *timer1Sec;
@property NSTimer *timerForLabelShowingRecordingTime;
@property (nonatomic, strong) IBOutlet UIImageView *camView;
@property (nonatomic, strong) CIContext *context;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property NSTimer *questionTimer;

@end

@implementation ChampCameraViewController
@synthesize Ball;
@synthesize myFrame;
@synthesize cameraIcon;
@synthesize secondToStart;
@synthesize infoCircle;
@synthesize infoRect;
@synthesize infoLabel;
@synthesize backgroundImage;
@synthesize infoButton;
@synthesize questionOne;
@synthesize questionTwo;
@synthesize questionThree;
@synthesize questionFour;
@synthesize questOne;
@synthesize questTwo;
@synthesize questThree;
@synthesize questFour;
@synthesize arrow;
@synthesize ballImagesArray;
@synthesize timer1Sec=_timer1Sec;
@synthesize elementsArray;
@synthesize questionsArray;
@synthesize nextQuestion;
@synthesize questionTimer;
@synthesize cameraImages;

@synthesize camView = _camView;
@synthesize context = _context;
@synthesize timerLabel;
@synthesize timerForLabelShowingRecordingTime;

- (CIContext *) context
{
    if (!_context) 
    {
        _context = [CIContext contextWithOptions:nil];
    }
    return _context;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark Buttons actions 

- (IBAction)skipButtonPressed 
{
    [self stopRecording];
    
    [self removeFile:movieURL];
    
    movieURL = nil;

    [self performSegueWithIdentifier:@"Show Menu" sender:self];
}


- (IBAction)backButtonPressed:(id)sender 
{
    [self stopRecording];

    [self removeFile:movieURL];
    
    movieURL = nil;

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonePressed:(id)sender 
{
    [self stopRecording];
    
    [self removeFile:movieURL];
    
    movieURL = nil;
    
    [self.navigationController popToViewController:[self.navigationController.childViewControllers objectAtIndex:0] animated:YES];
}

- (IBAction)startCapturing:(id)sender 
{
    [self startRecording];
}
- (IBAction)stopCapturing:(id)sender 
{
    [self stopRecording];
    
    [self saveMovieToCameraRoll];
    
    [self performSegueWithIdentifier:@"Show Menu" sender:self];
}

- (IBAction)infoButtonClick:(id)sender 
{
    [AnimationCoreModel fadeTimingAnimation:sender duration:0.125 repeatCount:1 autoreverses:YES];
    
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"infoViewController"] animated:YES];
}

#pragma mark View methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceUp || [[UIDevice currentDevice]orientation] == UIDeviceOrientationFaceDown) {
        CGRect rect=self.view.frame;
        [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+266 y:rect.size.height-86];
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:questOne,questTwo,questThree,questFour, nil] duration:0.125 x:0.8 y:0.8];
    }
    
    
    self.infoLabel.text = NSLocalizedString(@"startingVideoCaptureIn", nil);
    self.questionOne.text = NSLocalizedString(@"questionOne", nil);
    self.questionTwo.text = NSLocalizedString(@"questionTwo", nil);
    self.questionThree.text = NSLocalizedString(@"questionThree", nil);
    self.questionFour.text = NSLocalizedString(@"singOle", nil);
    
    self.timer1Sec=[NSTimer scheduledTimerWithTimeInterval:1.0
                                                    target:self
                                                  selector:@selector(changeTimer)
                                                  userInfo:nil 
                                                   repeats:YES]; 
    
    cameraIcon.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"kamera.png"],[UIImage imageNamed:@"kamera_2.png"], nil];
    cameraIcon.animationDuration=1;
    
    [self myinit];

}

- (void)viewDidUnload
{
    [self setBall:nil];
    [self setSecondToStart:nil];
    [self setInfoCircle:nil];
    [self setInfoRect:nil];
    [self setInfoLabel:nil];
    [self setBackgroundImage:nil];
    [self setInfoButton:nil];
    [self setQuestionOne:nil];
    [self setQuestionTwo:nil];
    [self setQuestionThree:nil];
    [self setQuestionFour:nil];
    [self setQuestOne:nil];
    [self setQuestTwo:nil];
    [self setQuestThree:nil];
    [self setQuestFour:nil];
    [self setArrow:nil];
    [self setTimerLabel:nil];
    [self setCameraIcon:nil];
    [self setMyFrame:nil];
    [super viewDidUnload];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (toInterfaceOrientation==UIDeviceOrientationLandscapeLeft ||
        toInterfaceOrientation==UIDeviceOrientationLandscapeRight ||
        toInterfaceOrientation==UIDeviceOrientationFaceUp ||
        toInterfaceOrientation==UIDeviceOrientationFaceDown)
    {
        backgroundImage.image=[UIImage imageNamed:@"gorizontal_fon.png"];
        myFrame.image=[UIImage imageNamed:@"ramka_2.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:questOne,questTwo,questThree,questFour, nil] duration:0.125 x:0.8 y:0.8];
    }
    else {
        myFrame.image=[UIImage imageNamed:@"ramka_1.png"];
        backgroundImage.image=[UIImage imageNamed:@"fon_1.png"];
        [AnimationCoreModel setScaleForElement:[NSArray arrayWithObjects:questOne,questTwo,questThree,questFour, nil] duration:0.125 x:1.0 y:1.0];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    CGRect rect=self.view.frame;
    [AnimationCoreModel moveElement:arrow duration:0.05 x:rect.origin.x+366 y:rect.size.height-86];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Menu"])
    {
        [segue.destinationViewController setMovieURL:movieURL];
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.timer1Sec invalidate];
    [self.timerForLabelShowingRecordingTime invalidate];
    [self.questionTimer invalidate];
    [self stopAndTearDownCaptureSession];
    [self.Ball stopAnimating];
    [self.cameraIcon stopAnimating];
}

- (void) viewWillAppear:(BOOL)animated
{
    startCount=STARTTIME;//-1;//9;
    startVideoRecord=NO;
    self.nextQuestion=YES;
    isRecordingNeedsToPause = NO;
    
    time=QUESTIONTIME;
    delay=DELAY;
    question=QUESTIONCOUNT;
    
    seconds =35;
    miliseconds=0;
    
    elementsArray=[NSArray arrayWithObjects:self.questFour,self.questionFour,self.questThree,self.questionThree,self.questTwo,self.questionTwo,self.questOne,self.questionOne, nil];
    questionsArray =[NSArray arrayWithObjects:self.questionFour.text,self.questionThree.text,self.questionTwo.text,self.questionOne.text, nil];
    
    [AnimationCoreModel fadeInAllElements:[NSArray arrayWithObjects:infoRect,infoCircle,infoLabel,secondToStart, nil] duration:6.0 alpha:0.0];
    
    self.infoCircle.frame=CGRectMake(infoCircle.frame.origin.x, infoCircle.frame.origin.y+60, infoCircle.frame.size.width, infoCircle.frame.size.height);
    self.secondToStart.frame=CGRectMake(secondToStart.frame.origin.x, secondToStart.frame.origin.y+60, secondToStart.frame.size.width, secondToStart.frame.size.height);

    [AnimationCoreModel animateBallElement:Ball duration:5.0];

    //[self stopAndTearDownCaptureSession];
    
    [self setupAndStartCaptureSession];
}

#pragma mark Timers

-(void)questionLoader
{
    if(self.nextQuestion)
    {
        if (question==0) {
            time=QUESTIONTIME;
            delay=DELAY;
            question=QUESTIONCOUNT;
            [self.questionTimer invalidate];
            self.questionTimer=nil;
            [self.timerForLabelShowingRecordingTime invalidate];
            self.timerForLabelShowingRecordingTime = nil;
            seconds=0;
        }
        else {
            [AnimationCoreModel questionAnimationOn:[NSArray arrayWithObjects:[elementsArray objectAtIndex:question*2-2],[elementsArray objectAtIndex:question*2-1], nil] duration:1.5 x:1.2 y:1.2 alpha:0.7];
            self.nextQuestion=NO;
            seconds=DELAY;
            self.timerForLabelShowingRecordingTime = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(changeMilicondsTimer) userInfo:nil repeats:YES];
        }        
    }
    delay--;
    if (delay<0) {
        if (delay==-1) {
            //[self resumeRecording];
            [self.timerForLabelShowingRecordingTime invalidate];
            self.timerForLabelShowingRecordingTime=nil;
            self.timerLabel.text=@"00:00";
            seconds=QUESTIONTIME;
            seconds--;
            self.timerForLabelShowingRecordingTime = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(changeMilicondsTimer) userInfo:nil repeats:YES];
            [cameraIcon startAnimating];
        }
        time--;
        if (time==0) {
            [AnimationCoreModel questionAnimationOff:[NSArray arrayWithObjects:[elementsArray objectAtIndex:question*2-2],[elementsArray objectAtIndex:question*2-1], nil] duration:1.5 x:1.0 y:1.0 alpha:0.0];
            question--;
            self.nextQuestion=YES;
            time=QUESTIONTIME;
            delay=DELAY;
            [self.timerForLabelShowingRecordingTime invalidate];
            self.timerForLabelShowingRecordingTime=nil;
            self.timerLabel.text=@"00:00";
            [cameraIcon stopAnimating];
            //[self pauseRecording];
        }
    }
}

-(void) changeMilicondsTimer
{
    NSString *secondsInString = seconds<10 && seconds>=0 ? [NSString stringWithFormat:@"0%d",seconds] : [NSString stringWithFormat:@"%d",seconds];
    NSString *milisecondsInString = [NSString stringWithFormat:@"%d0",miliseconds];
    
    self.timerLabel.text = [NSString stringWithFormat:@"%@:%@",secondsInString,milisecondsInString];
    
    if (miliseconds==0) 
    {
        seconds--;
        miliseconds=9;
    }
    
    miliseconds--;
}

-(void)changeTimer{
    
    if(!startVideoRecord){
        if(startCount>-1){
            self.secondToStart.text=[NSString stringWithFormat:@"%d",startCount];
            startCount--;
        }else if(startCount==-1){
            
            [AnimationCoreModel moveElement:backgroundImage duration:0.5 x:-797 y:917.5];
            
            startCount--;
            startVideoRecord=YES;
            startCount=9;
            
            self.questionTimer=[NSTimer scheduledTimerWithTimeInterval:1.0
                                                            target:self
                                                          selector:@selector(questionLoader)
                                                          userInfo:nil 
                                                           repeats:YES];
            [self startRecording];
        }
    }else
        if (self.questionTimer==nil) {
            [self.timer1Sec invalidate];
            self.timer1Sec=nil;
            startVideoRecord=NO;
            [self stopCapturing:self];
        }    
}

#pragma mark Work with camera

- (void) startRecording
{
	dispatch_async(movieWritingQueue, ^{
        
		// Remove the file if one with the same name already exists
		[self removeFile:movieURL];
        
		// Create an asset writer
		NSError *error;
		assetWriter = [[AVAssetWriter alloc] initWithURL:movieURL fileType:(NSString *)kUTTypeQuickTimeMovie error:&error];
        
	});	
}

- (void) stopRecording
{	
	dispatch_async(movieWritingQueue, ^{
                
		if ([assetWriter finishWriting]) 
        {
			/*[assetWriterAudioIn release];
             [assetWriterVideoIn release];
             [assetWriter release];*/
            assetWriterAudioIn = nil;
            assetWriterVideoIn = nil;
			assetWriter = nil;
            videoConnection =nil;
            audioConnection = nil;
			
			readyToRecordVideo = NO;
			readyToRecordAudio = NO;
			
		}
        
	});
    
}

- (void)saveMovieToCameraRoll
{
	ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:movieURL
								completionBlock:^(NSURL *assetURL, NSError *error) {
									if (error)
                                        error = nil;
															}];
	
}

- (void) writeSampleBuffer:(CMSampleBufferRef)sampleBuffer ofType:(NSString *)mediaType
{
	if ( assetWriter.status == AVAssetWriterStatusUnknown ) {
		
        if ([assetWriter startWriting]) {			
			[assetWriter startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
		}

	}
	
    NSError *error;
	if ( assetWriter.status == AVAssetWriterStatusWriting ) {
		
		if (mediaType == AVMediaTypeVideo) {
			if (assetWriterVideoIn.readyForMoreMediaData) {
				if (![assetWriterVideoIn appendSampleBuffer:sampleBuffer]) {
                    error = nil;
				}
			}
		}
		else if (mediaType == AVMediaTypeAudio) {
			if (assetWriterAudioIn.readyForMoreMediaData) {
				if (![assetWriterAudioIn appendSampleBuffer:sampleBuffer]) {
                    error = nil;
				}
			}
		}
	}
}

- (BOOL) setupCaptureSession 
{
    if (!captureSession)
        captureSession = [[AVCaptureSession alloc] init];
    
    /*
	 * Create audio connection
	 */
    
    AVCaptureDeviceInput *audioIn = [[AVCaptureDeviceInput alloc] initWithDevice:[self audioDevice] error:nil];
    if ([captureSession canAddInput:audioIn])
        [captureSession addInput:audioIn];
	
	AVCaptureAudioDataOutput *audioOut = [[AVCaptureAudioDataOutput alloc] init];
	dispatch_queue_t audioCaptureQueue = dispatch_queue_create("Audio Capture Queue", DISPATCH_QUEUE_SERIAL);
	[audioOut setSampleBufferDelegate:self queue:audioCaptureQueue];
	dispatch_release(audioCaptureQueue);
	if ([captureSession canAddOutput:audioOut])
		[captureSession addOutput:audioOut];
	audioConnection = [audioOut connectionWithMediaType:AVMediaTypeAudio];
    
	/*
	 * Create video connection
	 */
    AVCaptureDeviceInput *videoIn = [[AVCaptureDeviceInput alloc] initWithDevice:[self videoDeviceWithPosition:AVCaptureDevicePositionFront] error:nil];
    if ([captureSession canAddInput:videoIn])
        [captureSession addInput:videoIn];
    
    AVCaptureVideoDataOutput *videoOut = [[AVCaptureVideoDataOutput alloc] init];
	[videoOut setAlwaysDiscardsLateVideoFrames:YES];
	[videoOut setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
	dispatch_queue_t videoCaptureQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
	//[videoOut setSampleBufferDelegate:self queue:videoCaptureQueue];
    [videoOut setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
	dispatch_release(videoCaptureQueue);
	if ([captureSession canAddOutput:videoOut])
		[captureSession addOutput:videoOut];
	videoConnection = [videoOut connectionWithMediaType:AVMediaTypeVideo];
	videoConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
    //self.videoOrientation = [videoConnection videoOrientation];
    
	return YES;
}

- (void) setupAndStartCaptureSession
{
    
	movieWritingQueue = dispatch_queue_create("Movie Writing Queue", DISPATCH_QUEUE_SERIAL);
	
    if ( !captureSession )
		[self setupCaptureSession];
	
	
	if ( !captureSession.isRunning )
		[captureSession startRunning];
}

- (BOOL) setupAssetWriterAudioInput:(CMFormatDescriptionRef)currentFormatDescription
{
	const AudioStreamBasicDescription *currentASBD = CMAudioFormatDescriptionGetStreamBasicDescription(currentFormatDescription);
    
	size_t aclSize = 0;
	const AudioChannelLayout *currentChannelLayout = CMAudioFormatDescriptionGetChannelLayout(currentFormatDescription, &aclSize);
	NSData *currentChannelLayoutData = nil;
	
	// AVChannelLayoutKey must be specified, but if we don't know any better give an empty data and let AVAssetWriter decide.
	if ( currentChannelLayout && aclSize > 0 )
		currentChannelLayoutData = [NSData dataWithBytes:currentChannelLayout length:aclSize];
	else
		currentChannelLayoutData = [NSData data];
	
	NSDictionary *audioCompressionSettings = [NSDictionary dictionaryWithObjectsAndKeys:
											  [NSNumber numberWithInteger:kAudioFormatMPEG4AAC], AVFormatIDKey,
											  [NSNumber numberWithFloat:currentASBD->mSampleRate], AVSampleRateKey,
											  [NSNumber numberWithInt:64000], AVEncoderBitRatePerChannelKey,
											  [NSNumber numberWithInteger:currentASBD->mChannelsPerFrame], AVNumberOfChannelsKey,
											  currentChannelLayoutData, AVChannelLayoutKey,
											  nil];
	if ([assetWriter canApplyOutputSettings:audioCompressionSettings forMediaType:AVMediaTypeAudio]) {
		assetWriterAudioIn = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeAudio outputSettings:audioCompressionSettings];
		assetWriterAudioIn.expectsMediaDataInRealTime = YES;
		if ([assetWriter canAddInput:assetWriterAudioIn])
			[assetWriter addInput:assetWriterAudioIn];
		else {
			NSLog(@"Couldn't add asset writer audio input.");
            return NO;
		}
	}
	else {
		NSLog(@"Couldn't apply audio output settings.");
        return NO;
	}
    
    return YES;
}

- (BOOL) setupAssetWriterVideoInput:(CMFormatDescriptionRef)currentFormatDescription 
{
	float bitsPerPixel;
	CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(currentFormatDescription);
	int numPixels = dimensions.width * dimensions.height;
	int bitsPerSecond;
	
	// Assume that lower-than-SD resolutions are intended for streaming, and use a lower bitrate
	if ( numPixels < (640 * 480) )
		bitsPerPixel = 4.05; // This bitrate matches the quality produced by AVCaptureSessionPresetMedium or Low.
	else
		bitsPerPixel = 11.4; // This bitrate matches the quality produced by AVCaptureSessionPresetHigh.
	
	bitsPerSecond = numPixels * bitsPerPixel;
	
	NSDictionary *videoCompressionSettings = [NSDictionary dictionaryWithObjectsAndKeys:
											  AVVideoCodecH264, AVVideoCodecKey,
											  [NSNumber numberWithInteger:dimensions.width], AVVideoWidthKey,
											  [NSNumber numberWithInteger:dimensions.height], AVVideoHeightKey,
											  [NSDictionary dictionaryWithObjectsAndKeys:
											   [NSNumber numberWithInteger:bitsPerSecond], AVVideoAverageBitRateKey,
											   [NSNumber numberWithInteger:30], AVVideoMaxKeyFrameIntervalKey,
											   nil], AVVideoCompressionPropertiesKey,
											  nil];
	if ([assetWriter canApplyOutputSettings:videoCompressionSettings forMediaType:AVMediaTypeVideo]) {
		assetWriterVideoIn = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoCompressionSettings];
		assetWriterVideoIn.expectsMediaDataInRealTime = YES;
        
        UIInterfaceOrientation currentOrientation;
        switch (self.interfaceOrientation) {
            case UIInterfaceOrientationLandscapeRight:
                currentOrientation = UIInterfaceOrientationLandscapeLeft;
                break;
                
            case UIInterfaceOrientationLandscapeLeft:
                currentOrientation = UIInterfaceOrientationLandscapeRight;
                break;
                
            case UIInterfaceOrientationPortrait:
                currentOrientation = UIInterfaceOrientationPortrait;
                break;
                
            case UIInterfaceOrientationPortraitUpsideDown:
                currentOrientation = UIInterfaceOrientationPortraitUpsideDown;
                break;
                
            default:
                currentOrientation = UIInterfaceOrientationPortrait;
                break;
        }
        
        assetWriterVideoIn.transform = [self transformFromCurrentVideoOrientationToOrientation:currentOrientation];
		if ([assetWriter canAddInput:assetWriterVideoIn])
			[assetWriter addInput:assetWriterVideoIn];
		else {
			NSLog(@"Couldn't add asset writer video input.");
            return NO;
		}
	}
	else {
		NSLog(@"Couldn't apply video output settings.");
        return NO;
	}
    
    return YES;
}

#pragma mark Capture

- (void)captureOutput:(AVCaptureOutput *)captureOutput 
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer 
       fromConnection:(AVCaptureConnection *)connection 
{	
	CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
    
    CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    CGImageRef ref = [self.context createCGImage:ciImage fromRect:ciImage.extent];
    
    UIImageOrientation currentImageOrientation;
    switch (self.interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
            currentImageOrientation = UIImageOrientationUp;
            break;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            currentImageOrientation = UIImageOrientationDown;
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
            currentImageOrientation = UIImageOrientationLeft;
            break;
        
        case UIInterfaceOrientationLandscapeRight:
            currentImageOrientation = UIImageOrientationRight;
            break;
            
        default:
            currentImageOrientation = UIImageOrientationUp;
            break;
    }
    
    self.camView.image = [UIImage imageWithCGImage:ref scale:1.0 orientation:currentImageOrientation];
    
    CGImageRelease(ref);
    
	CFRetain(sampleBuffer);
	CFRetain(formatDescription);
    //if (!isRecordingNeedsToPause)
    //{
	dispatch_async(movieWritingQueue, ^{
        
		if ( assetWriter ) {
            	
			if (connection == videoConnection) {
                
				// Initialize the video input if this is not done yet
				if (!readyToRecordVideo)
					readyToRecordVideo = [self setupAssetWriterVideoInput:formatDescription];
				
				// Write video data to file
				if (readyToRecordVideo && readyToRecordAudio)
					[self writeSampleBuffer:sampleBuffer ofType:AVMediaTypeVideo];
			}
			else if (connection == audioConnection) {
				
				// Initialize the audio input if this is not done yet
				if (!readyToRecordAudio)
					readyToRecordAudio = [self setupAssetWriterAudioInput:formatDescription];
				
				// Write audio data to file
				if (readyToRecordAudio && readyToRecordVideo)
					[self writeSampleBuffer:sampleBuffer ofType:AVMediaTypeAudio];
			}

		}
		CFRelease(sampleBuffer);
		CFRelease(formatDescription);
	});
    //}
}

- (AVCaptureDevice *)videoDeviceWithPosition:(AVCaptureDevicePosition)position 
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
        if ([device position] == position)
            return device;
    
    return nil;
}

- (AVCaptureDevice *)audioDevice
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
    if ([devices count] > 0)
        return [devices objectAtIndex:0];
    
    return nil;
}

- (void) pauseRecording
{
    isRecordingNeedsToPause = YES;
    if (captureSession.isRunning)
    {
        [captureSession stopRunning];
    }
    
}

- (void) resumeRecording
{
	isRecordingNeedsToPause = NO;
    
    if (!captureSession.isRunning)
    {
        [captureSession startRunning];
    }
}

- (void) stopAndTearDownCaptureSession
{
    if ([captureSession isRunning]) 
    {
        [captureSession stopRunning];
    }
    
	captureSession = nil;
	audioConnection = nil;
	videoConnection = nil;
	
	//assetWriter = nil;
	assetWriterAudioIn = nil;
	assetWriterVideoIn = nil;
	
	if (movieWritingQueue) 
    {
		dispatch_release(movieWritingQueue);
		movieWritingQueue = NULL;
    }
}

#pragma mark Utilities

- (void)removeFile:(NSURL *)fileURL
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [fileURL path];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
		if (!success)
			error = nil;
    }
}

- (CGFloat)angleOffsetFromPortraitOrientationToOrientation:(AVCaptureVideoOrientation)orientation
{
	CGFloat angle = 0.0;
	
	switch (orientation) {
		case AVCaptureVideoOrientationPortrait:
			angle = 0.0;
			break;
		case AVCaptureVideoOrientationPortraitUpsideDown:
			angle = M_PI;
			break;
		case AVCaptureVideoOrientationLandscapeRight:
			angle = -M_PI_2;
			break;
		case AVCaptureVideoOrientationLandscapeLeft:
			angle = M_PI_2;
			break;
		default:
			break;
	}
    
	return angle;
}


- (CGAffineTransform)transformFromCurrentVideoOrientationToOrientation:(AVCaptureVideoOrientation)orientation
{
	CGAffineTransform transform = CGAffineTransformIdentity;
    
	// Calculate offsets from an arbitrary reference orientation (portrait)
	CGFloat orientationAngleOffset = [self angleOffsetFromPortraitOrientationToOrientation:orientation];
	//CGFloat videoOrientationAngleOffset = [self angleOffsetFromPortraitOrientationToOrientation:self.videoOrientation];
	
	// Find the difference in angle between the passed in orientation and the current video orientation
	CGFloat angleOffset = orientationAngleOffset; //- videoOrientationAngleOffset;
	transform = CGAffineTransformMakeRotation(angleOffset);
	
	return transform;
}

- (void) myinit
{        
    movieURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), @"Movie.MOV"]];
}


@end
